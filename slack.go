package slack

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

// Slack to store the env & config values related to slack
type Slack struct {
	UserToken string
	URL       string
}

// Initialize initializes the slack struct with the env values
func (s *Slack) Initialize() {
	s.UserToken = os.Getenv("SLACKUSERTOKEN")
	if s.UserToken == "" {
		log.Fatalf("ERROR: Setting SLACKUSERTOKEN Env variable")
	}
	if s.URL == "" {
		s.URL = "https://slack.com/api/chat.postMessage"
	}
}

// CreateSlackMessage creates a slack message to the given channel with the given message
func (s *Slack) CreateSlackMessage(channel, message string) {
	body := map[string]interface{}{"channel": channel, "text": message, "username": "Full Stack"}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		log.Println("ERROR: Unable to marshal body", err)
	}
	bearer := "Bearer " + s.UserToken
	req, err := http.NewRequest("POST", s.URL, bytes.NewBuffer(jsonBody))
	if err != nil {
		log.Println("ERROR: Unable to create a new request", err)
	}
	req.Header.Set("Authorization", bearer)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR: Unable to perform POST request", err)
	}
	defer resp.Body.Close()
}

// Channel to store the channel id of individual channels
type Channel struct {
	Notifications     string
	APILogs           string
	InviteCodeRequest string
}

// Set sets SlackChannel struct with the env values
func (c *Channel) Set(name string) {
	switch name {
	case "notifications":
		c.Notifications = os.Getenv("CHANNELNOTIFICATIONS")
		if c.Notifications == "" {
			log.Fatalf("FATAL: Setting SLACKNOTIFICATIONS Env variable")
		}
	case "apilogs":
		c.APILogs = os.Getenv("CHANNELAPILOGS")
		if c.APILogs == "" {
			log.Fatalf("FATAL: Setting CHANNELAPILOGS Env variable")
		}
	case "invitecoderequest":
		c.InviteCodeRequest = os.Getenv("CHANNELINVITECODEREQUEST")
		if c.InviteCodeRequest == "" {
			log.Fatalf("FATAL: Setting CHANNELINVITECODEREQUEST Env variable")
		}
	}
}
